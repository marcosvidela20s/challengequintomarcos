package com.ChallengeQuinto.core.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ChallengeQuinto.core.entity.Curso;
import com.ChallengeQuinto.core.service.AlumnoService;
import com.ChallengeQuinto.core.service.CursoService;

@Controller
@RequestMapping("/alumno")
public class AlumnoController {
	
	@Autowired
	private AlumnoService alumnoService;
	
	@Autowired
	private CursoService cursoService;
	
	@GetMapping("/crear")
	public String crearAlumno(ModelMap modelo ) {
		List<Curso> cursos =cursoService.listarCursos();
		
		modelo.addAttribute("cursos", cursos);
		
		return "alumno_form.html";
	}
	
	
	@PostMapping("/creado")
	public String creadoAlumno (@RequestParam String nombreAlumno, Integer edad, Date fechananimiento,String historia,String idCurso) {
		alumnoService.crearAlumno(nombreAlumno, edad, fechananimiento, historia, idCurso);
		
		return "index.html";

	}

}
