package com.ChallengeQuinto.core.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ChallengeQuinto.core.entity.Curso;
import com.ChallengeQuinto.core.entity.Profesor;
import com.ChallengeQuinto.core.service.CursoService;
import com.ChallengeQuinto.core.service.ProfesorService;

@Controller
@RequestMapping("/curso")
public class CursoController {
	
	@Autowired
	private CursoService cursoService;
	@Autowired
	private ProfesorService profesorService;
	
	@GetMapping("/crear")
	
	public String crearCurso(ModelMap modelo ) {
		List<Profesor> profesores =profesorService.listarProfesores();
		
		modelo.addAttribute("profesores", profesores);
		
		return "curso_form.html";
	}
	
	@PostMapping("/creado")
	
	public String creadoCurso (@RequestParam String nombreCurso, String idProfesor, String turno, Date horario,String idAlumnos) {
		cursoService.crearCurso(nombreCurso, idProfesor, turno, horario, idAlumnos);
		
		return "index.html";

	}

}
