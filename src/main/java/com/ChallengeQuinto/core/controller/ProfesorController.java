package com.ChallengeQuinto.core.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ChallengeQuinto.core.entity.Curso;
import com.ChallengeQuinto.core.entity.Profesor;
import com.ChallengeQuinto.core.service.CursoService;
import com.ChallengeQuinto.core.service.ProfesorService;

@Controller
@RequestMapping("/profesor")
public class ProfesorController {

	@Autowired
	private ProfesorService profesorService;
	
	@Autowired
	private CursoService cursoService;

	@GetMapping("/crear")
	public String crearProfesor(ModelMap modelo ) {
		List<Curso> cursos =cursoService.listarCursos();
		modelo.addAttribute("cursos", cursos);
		return "profesor_form.html";
	}
	
	
	@PostMapping("/creado")
	public String creadoProfesor (@RequestParam  String nombreProfesor,String apellidoProfesor, String idCurso) {
		profesorService.crearProfesor(nombreProfesor, apellidoProfesor, idCurso);
		return "index.html";
	}
	
	@GetMapping("/listar")
	public String listarProfesores(ModelMap modelo ) {
		List<Profesor> profesores =profesorService.listarProfesores();
		modelo.addAttribute("profesores", profesores);
		return "profesor_list.html";
	}
	
}
