package com.ChallengeQuinto.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChallengebackendQuintoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChallengebackendQuintoApplication.class, args);
	}

}
