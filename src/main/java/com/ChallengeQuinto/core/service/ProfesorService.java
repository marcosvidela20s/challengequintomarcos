package com.ChallengeQuinto.core.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ChallengeQuinto.core.entity.Alumno;
import com.ChallengeQuinto.core.entity.Curso;
import com.ChallengeQuinto.core.entity.Profesor;
import com.ChallengeQuinto.core.exceptions.TheExceptions;
import com.ChallengeQuinto.core.repository.CursoRepository;
import com.ChallengeQuinto.core.repository.ProfesorRepository;

import jakarta.transaction.Transactional;

@Service
public class ProfesorService {
	
	@Autowired
	private CursoRepository cursoRepository; 
	
	@Autowired
	
	private ProfesorRepository profesorRepository; 
	
	@Transactional
	public void crearProfesor(/*String id,*/ String nombreProfesor,String apellidoProfesor, String idCurso) {
		
		Profesor profesor = new Profesor();
		Curso curso = cursoRepository.findByIdCurso(idCurso)/*.get() PREGUNTAR*/;
		
		/*profesor.setId(id);*/
		profesor.setNombreProfesor(nombreProfesor);
		profesor.setApellidoProfesor(apellidoProfesor);
		profesor.setCursos(curso);
		
		profesorRepository.save(profesor);

		
	}
	
	public List<Profesor> listarProfesores(){
		
		List<Profesor> profesores = new ArrayList();
		profesores = profesorRepository.findAll();
		return profesores;
		
	}
	@Transactional
	public void editarProfesor(String id, String nombreProfesor,String apellidoProfesor, String idCurso) throws TheExceptions {
		
		if(id.isEmpty()|| id==null) {
			throw new TheExceptions("Este profesor no existe");
		}
		
		Optional<Profesor> respuesta = profesorRepository.findById(id);
		Optional<Curso> respuestaCurso = cursoRepository.findById(idCurso);
		
		Curso curso = new Curso();
		
		if(respuestaCurso.isPresent()) {
			curso = respuestaCurso.get();
		}
		
		if (respuesta.isPresent()) {
			Profesor profesor =respuesta.get();	
			
			profesor.setNombreProfesor(nombreProfesor);
			profesor.setApellidoProfesor(apellidoProfesor);
			profesor.setCursos(curso);
			
			profesorRepository.save(profesor);
		}
		
	
	}
	@Transactional
	public void eliminarProfesor(String id) throws TheExceptions {
		
		if(id.isEmpty()|| id==null) {
			throw new TheExceptions("Este profesor no existe");
		}
		
		Optional<Profesor> respuesta = profesorRepository.findById(id);

		if (respuesta.isPresent()) {
			Profesor profesor =respuesta.get();
			
			profesorRepository.delete(profesor);
		}
	}
	
	@Transactional
	public void eliminarProfesorCurso(String id,String idCurso) {
		Optional<Profesor> respuesta = profesorRepository.findById(id);
		Optional<Curso> respuestaCurso = cursoRepository.findById(idCurso);
		
		Curso curso = new Curso();
		
		if(respuestaCurso.isPresent()) {
			curso = respuestaCurso.get();
		}
		
		if (respuesta.isPresent()) {
			Profesor profesor =respuesta.get();
			
			profesorRepository.delete(profesor.curso); /*preguntar*/
		}
	}
	
}
