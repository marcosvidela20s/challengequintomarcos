package com.ChallengeQuinto.core.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ChallengeQuinto.core.entity.Alumno;
import com.ChallengeQuinto.core.entity.Curso;
import com.ChallengeQuinto.core.exceptions.TheExceptions;
import com.ChallengeQuinto.core.repository.AlumnoRepository;
import com.ChallengeQuinto.core.repository.CursoRepository;

import jakarta.transaction.Transactional;

@Service
public class AlumnoService {
	
	@Autowired
	private AlumnoRepository alumnoRepository;
	
	@Autowired
	private CursoRepository cursoRepository; 
	
	@Autowired
	
	private CursoService cursoService; 
	
	@Transactional
	public void crearAlumno(/* String id,*/ String nombreAlumno, Integer edad, Date fechananimiento,String historia,String idCurso) {
		
		Alumno alumno = new Alumno();
		
		/*alumno.setId(id);*/
		alumno.setNombreAlumno(nombreAlumno);
		alumno.setEdad(edad);
		alumno.setFechananimiento(fechananimiento);
		alumno.setHistoria(historia);
		
		alumnoRepository.save(alumno);
		cursoService.agregarAlumno(idCurso, alumno);
				
	}
	
	public List<Alumno> listarAlumnos(){
		
		List<Alumno> alumnos = new ArrayList();
		alumnos = alumnoRepository.findAll();
		return alumnos;
		
	}
	
	@Transactional
	public void editarAlumno(String id, String nombreAlumno, Integer edad, Date fechananimiento,String historia) throws TheExceptions {
		
		if(id.isEmpty()|| id==null) {
			throw new TheExceptions("este alumno no existe");
		}
		
		Optional<Alumno> respuesta = alumnoRepository.findById(id);
		
		Curso curso = new Curso();
		
		if (respuesta.isPresent()) {
			Alumno alumno =respuesta.get();
			
			alumno.setNombreAlumno(nombreAlumno);
			alumno.setEdad(edad);
			alumno.setFechananimiento(fechananimiento);
			alumno.setHistoria(historia);
			
			alumnoRepository.save(alumno);

		}

	}
	
	@Transactional
	public void eliminarAlumno(String id) throws TheExceptions {
		
		if(id.isEmpty()|| id==null) {
			throw new TheExceptions("este alumno no existe");
		}
		
		Optional<Alumno> respuesta = alumnoRepository.findById(id);
		
		if (respuesta.isPresent()) {
			Alumno alumno =respuesta.get();
			
			alumnoRepository.delete(alumno);
		}
	}
	
	
}
