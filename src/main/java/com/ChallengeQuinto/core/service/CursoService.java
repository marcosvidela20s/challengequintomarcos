package com.ChallengeQuinto.core.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ChallengeQuinto.core.entity.Alumno;
import com.ChallengeQuinto.core.entity.Curso;
import com.ChallengeQuinto.core.entity.Profesor;
import com.ChallengeQuinto.core.enums.Turno;
import com.ChallengeQuinto.core.exceptions.TheExceptions;
import com.ChallengeQuinto.core.repository.AlumnoRepository;
import com.ChallengeQuinto.core.repository.CursoRepository;
import com.ChallengeQuinto.core.repository.ProfesorRepository;

import jakarta.transaction.Transactional;

@Service
public class CursoService {
	
	@Autowired
	
	private CursoRepository cursoRepository;

	
	@Autowired
	private AlumnoRepository alumnoRepository;
	@Autowired

	private ProfesorRepository profesorRepository;
	
	
	@Transactional
	public void crearCurso(/*String id,*/ String nombreCurso, String idProfesor, String turno, Date horario,String idAlumno) {
		
		Curso curso = new Curso();
		Profesor profesor = profesorRepository.findByIdProfesor(idProfesor)/*.get()* PREGUNTAR*/;
	
		/*curso.setId(id);*/
		curso.setNombreCurso(nombreCurso);
		curso.setProfesor(profesor);
		
		if (turno.equals("Mañana")) { 
			curso.setTurno(Turno.MANIANA);
		}
		curso.setTurno(Turno.MANIANA);//evaluar el string turno y en base a eso transformalo en enum IF//
		curso.setHorario(horario);
		
		cursoRepository.save(curso);

		
	}
	
	public void agregarAlumno(String idCurso, Alumno alumno) {
		Curso curso= cursoRepository.findByIdCurso(idCurso);
		List<Alumno> alumonos= curso.getAlumnos();
		alumonos.add(alumno);
		curso.setAlumnos(alumonos);
		cursoRepository.save(curso);
	}
		//llamar al curso correspondiente y setearle a su lista de alumnos el alumno correspondiente /

	public Curso agregarAlumnoDeEditarCurso(String idCurso, Alumno alumno) {
		Curso curso= cursoRepository.findByIdCurso(idCurso);
		List<Alumno> alumonos= curso.getAlumnos();
		alumonos.add(alumno);
		curso.setAlumnos(alumonos);
		return curso;
	}
	
	public List<Curso> listarCursos(){
		
		List<Curso> cursos = new ArrayList();
		cursos = cursoRepository.findAll();
		return cursos;
		
	}

	@Transactional
	public void editarCurso(String id, String nombreCurso, String idProfesor, String turno, Date horario,String idAlumnos) throws TheExceptions {
		
		if(id.isEmpty()|| id==null) {
			throw new TheExceptions("Este curso no existe");
		}
		
		
		Optional<Curso> respuesta = cursoRepository.findById(id);
		Optional<Profesor> respuestaProfesor = profesorRepository.findById(idProfesor);
		Optional<Alumno> respuestaAlumno = alumnoRepository.findById(idAlumnos); /*atencion preguntar*/
		
		Profesor profesor = new Profesor();
		Alumno alumnos = new Alumno(); 
		
		if(respuestaProfesor.isPresent()) {
			profesor = respuestaProfesor.get();
		}
		
		if(respuestaAlumno.isPresent()) {
			alumnos = respuestaAlumno.get();
		}
		
		if (respuesta.isPresent()) {
			Curso curso =respuesta.get();
			
			curso.setNombreCurso(nombreCurso);
			curso.setProfesor(profesor);
			curso.setTurno(turno);
			curso.setHorario(horario);
			curso.setAlumnos(alumnos);
			
			cursoRepository.save(curso);
			
		}
		
	}
	
	@Transactional
	public void eliminarCurso(String id) throws TheExceptions {
		
		if(id.isEmpty()|| id==null) {
			throw new TheExceptions("Este curso no existe");
		}
		
		Optional<Curso> respuesta = cursoRepository.findById(id);
		
		if (respuesta.isPresent()) {
			Curso curso =respuesta.get();
			
			cursoRepository.delete(curso);
		}
	}
}
