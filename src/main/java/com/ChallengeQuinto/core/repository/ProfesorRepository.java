package com.ChallengeQuinto.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ChallengeQuinto.core.entity.Profesor;

@Repository
public interface ProfesorRepository extends JpaRepository<Profesor, String> {
	
	@Query("SELECT p FROM Profesor p WHERE p.nombreProfesor = :nombre")
	public Profesor buscarPorNombreProfesor (@Param("nombreProfesor") String nombre);
	
	@Query("SELECT p FROM Profesor p WHERE p.apellidoProfesor = :apellido")
	public Profesor buscarPorApellidoProfesor (@Param("apellidoProfesor") String pellido);
	
	@Query("SELECT p FROM Profesor p WHERE p.id = :idProfesor")
	public Profesor findByIdProfesor (@Param("id") String idProfesor);

}
