package com.ChallengeQuinto.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ChallengeQuinto.core.entity.Curso;

@Repository
public interface CursoRepository extends JpaRepository <Curso, String>{

	@Query("SELECT c FROM Curso c WHERE c.nombreCurso = :nombre")
	public Curso buscarPorNombreCurso (@Param("nombreCurso") String nombre);
	
	@Query("SELECT a FROM Curso a WHERE a.id = :idCurso")
	public Curso findByIdCurso (@Param("id") String idCurso);
	
}
