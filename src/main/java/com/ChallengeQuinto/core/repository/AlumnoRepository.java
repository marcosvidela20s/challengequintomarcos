package com.ChallengeQuinto.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ChallengeQuinto.core.entity.Alumno;

@Repository
public interface AlumnoRepository extends JpaRepository <Alumno, String>{
	
	@Query("SELECT a FROM Alumno a WHERE a.nombreAlumno = :nombre")
	public Alumno buscarPorNombreAlumno (@Param("nombreAlumno") String nombre);
	
	@Query("SELECT a FROM Alumno a WHERE a.id = :id")
	public Alumno fyndByIdAlumno (@Param("id") String idAlumno);
	
	@Query("SELECT a FROM Alumno a WHERE a.curso.nombreCurso = :curso")	
	public List<Alumno> buscarPorCurso (@Param("nombreCurso") String curso);
	

}
