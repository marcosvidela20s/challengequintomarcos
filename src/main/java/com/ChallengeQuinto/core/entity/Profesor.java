package com.ChallengeQuinto.core.entity;

import java.util.List;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Id;

import com.ChallengeQuinto.core.enums.Rol;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.OneToMany;
import lombok.Data;

@Data
@Entity

public class Profesor {
	 @Id
	 @GeneratedValue(generator = "uuid")
	 @GenericGenerator(name = "uuid", strategy = "uuid2")
	 
	 private String id;
	 private String nombreProfesor;
	 private String apellidoProfesor;
	 @OneToMany
	 private List<Curso> cursos;
	 private Rol PROFESOR;/*PREGUNTAR*/

	 
	
}
