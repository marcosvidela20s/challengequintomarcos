package com.ChallengeQuinto.core.entity;

import java.util.Date;
import java.util.List;



import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Id;

import com.ChallengeQuinto.core.enums.Turno;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;


@Data
@Entity

public class Curso {
	 @Id
	 @GeneratedValue(generator = "uuid")
	 @GenericGenerator(name = "uuid", strategy = "uuid2")
	 private String id;
	 private String nombreCurso;
	 
	 @OneToOne
	 private Profesor profesor;
	 
	
	 private Turno turno; 
	 
	 @Temporal(TemporalType.TIME)
	 private Date horario;
	 
	 @OneToMany
	 private List<Alumno> alumnos;
	 
}
