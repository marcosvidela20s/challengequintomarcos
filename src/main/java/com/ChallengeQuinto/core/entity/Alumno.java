package com.ChallengeQuinto.core.entity;

import java.util.Date;

import org.hibernate.annotations.GenericGenerator;

import com.ChallengeQuinto.core.enums.Rol;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import lombok.Data;

@Data
@Entity
public class Alumno {
	 @Id
	 @GeneratedValue(generator = "uuid")
	 @GenericGenerator(name = "uuid", strategy = "uuid2")
	 private String id;
	 
	 private String nombreAlumno;
	 
	 private Integer edad;
	 
	 @Temporal(TemporalType.DATE)
	 private Date fechananimiento;
	 
	 private String historia;
		 
	 private Rol ALUMNO;/*PREGUNTAR*/
	 
}
